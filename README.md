# README #

Scaffold only.

### What is this repository for? ###

* Experimental
* Does not address patient confidentiality
* Does not address access control (e.g. role-based)
* Needs a full list of blood types (enum)
* Sets up CRUD operations for doners and donations

### How do I get set up? ###

* Make sure both files are loaded in Remix
* Use the OLD Remix interface. The new one is still quirky.

### Contribution guidelines ###

* Go nuts

### Who do I talk to? ###

* rob.hitchens.mobile@gmail.com. 
