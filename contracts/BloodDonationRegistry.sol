pragma solidity 0.5.1;

import "./HitchensUnorderedKeySet.sol";
import "./Ownable.sol";

/**
 * @dev Ownable restricts updates to one priviledge used such as a centralized server or registry. Consider a Whitelist or role-based access control for clinics, etc. 
 */

contract BloodDonationRegistry is Ownable {
    
    uint constant SHELF_LIFE = 90 days;
    
    using HitchensUnorderedKeySetLib for HitchensUnorderedKeySetLib.Set;
    HitchensUnorderedKeySetLib.Set personSet;
    mapping(bytes32 => PersonStruct) person; // personId => PersonStruct
    
    HitchensUnorderedKeySetLib.Set donationSet;
    mapping(bytes32 => DonationStruct) donation; // donationId => BloodDonationStruct
    
    enum BloodType {Op, On, Ap, An, etc} // enumerate all the blood types
    
    uint public nonce;
    
    struct PersonStruct {
        ProfileStruct profile;
        LongTermInformationStruct longTermInformation;
        ShortTermInformationStruct shortTermInformation;
        HitchensUnorderedKeySetLib.Set donationSet; // possibly the next phase has to do with recalls. This will hold donationId by person.
    }
    struct ProfileStruct {
        string name;
        uint dateOfBirth;
        uint weight;
    }
    struct LongTermInformationStruct {
        BloodType bloodType;
        bool allergies;
        bool diabetes;
        bool piercing;
        bool gay;
        bool tRLungInjury;
        bool drugs;
        bool cancer;
    }
    struct ShortTermInformationStruct {
        bool childBirth;
        bool cholestrol;
        bool dentist;
        bool soreThroat;
        bool tickBite;
        bool cold;
        bool vaccination;
    }
    struct DonationStruct {
        uint date;
        uint expiryDate;
        bytes32 doner;
        address signer;
        bool destroyed;
        // probably other info such as clinic, qty, ??
    }
    
    event LogNewPersonProfile(address sender, bytes32 personId, string name, uint dateOfBirth, uint weight);
    event LogUpdatePersonProfile(address sender, bytes32 personId, string name, uint dateOfBirth, uint weight);    
    event LogUpdateLongTermInformation(address sender, bytes32 personId, BloodType bloodType, bool allergies, bool diabetes, bool piercing, bool gay, bool tRLungInjury, bool drugs, bool cancer);
    event LogUpdateShortTermInformation(address sender, bytes32 personId, bool childBirth, bool cholestrol, bool dentist, bool soreThroat, bool tickBite, bool cold, bool vaccination);
    event LogDonation(address sender, bytes32 personId, uint expiryDate);
    event LogDonationDestroyed(address sender, bytes32 donationId);
    /**
     * @dev PersonProfiles
     */
    function genId() internal returns(bytes32) {
        return keccak256(abi.encodePacked(address(this), nonce++));
    }
    function newPersonProfile(string memory name, uint dateOfBirth, uint weight) public onlyOwner returns(bytes32 personId) {
        personId = genId();
        personSet.insert(personId);
        ProfileStruct storage p = person[personId].profile;
        p.name = name;
        p.dateOfBirth = dateOfBirth;
        p.weight = weight;
        emit LogNewPersonProfile(msg.sender, personId, name, dateOfBirth, weight);
    }
    function updatePersonProfile(bytes32 personId, string memory name, uint dateOfBirth, uint weight) public onlyOwner {
        require(personSet.exists(personId), "PersonId not found.");
        ProfileStruct storage p = person[personId].profile;
        p.name = name;
        p.dateOfBirth = dateOfBirth;
        p.weight = weight;
        emit LogUpdatePersonProfile(msg.sender, personId, name, dateOfBirth, weight);
    }
    function personProfile(bytes32 personId) public view returns(string memory nane, uint dateOfBirth, uint weight) {
        require(personSet.exists(personId), "Person not found");
        ProfileStruct storage p = person[personId].profile;
        return (p.name, p.dateOfBirth, p.weight);
    }
    function personProfileCount() public view returns(uint count) {
        return personSet.count();
    }
    function personAtIndex(uint index) public view returns(bytes32 personId) {
        return personSet.keyAtIndex(index);
    }
    
    /**
     * @dev Long-Term Information
     */
    function updateLongTermInformation(bytes32 personId, BloodType bloodType, bool allergies, bool diabetes, bool piercing, bool gay, bool tRLungInjury, bool drugs, bool cancer) public onlyOwner {
        require(personSet.exists(personId), "PersonId not found.");
        LongTermInformationStruct storage l = person[personId].longTermInformation;
        l.bloodType = bloodType;
        l.allergies = allergies;
        l.diabetes = diabetes;
        l.piercing = piercing;
        l.gay = gay;
        l.tRLungInjury = tRLungInjury;
        l.drugs = drugs;
        l.cancer = cancer;
        emit LogUpdateLongTermInformation(msg.sender, personId, bloodType, allergies, diabetes, piercing, gay, tRLungInjury, drugs, cancer);
    }
    function longTermInformation(bytes32 personId) public view returns(bool allergies, bool diabetes, bool piercing, bool gay, bool tRLungInjury, bool drugs, bool cancer) {
        require(personSet.exists(personId), "PersonId not found.");
        LongTermInformationStruct storage l = person[personId].longTermInformation;
        return(l.allergies, l.diabetes, l.piercing, l.gay, l.tRLungInjury, l.drugs, l.cancer);
    }
     
    /**
     * @dev Short-Term Infomration
     */
    function updateShortTermInformation(bytes32 personId, bool childBirth, bool cholestrol, bool dentist, bool soreThroat, bool tickBite, bool cold, bool vaccination) public onlyOwner {
        require(personSet.exists(personId), "PersonId not found.");
        ShortTermInformationStruct storage s = person[personId].shortTermInformation;
        s.childBirth = childBirth;
        s.cholestrol = cholestrol;
        s.dentist = dentist;
        s.soreThroat = soreThroat;
        s.tickBite = tickBite;
        s.cold = cold;
        s.vaccination = vaccination;
        emit  LogUpdateShortTermInformation(msg.sender, personId, childBirth, cholestrol, dentist, soreThroat, tickBite, cold, vaccination);
    }
    function shortTermInformation(bytes32 personId) public view returns(bool childBirth, bool cholestrol, bool dentist, bool soreThroat, bool tickBite, bool cold, bool vaccination) {
        require(personSet.exists(personId), "PersonId not found.");
        ShortTermInformationStruct storage s = person[personId].shortTermInformation;
        return(s.childBirth, s.cholestrol, s.dentist, s.soreThroat, s.tickBite, s.cold, s.vaccination);
    }
    
    /**
     * @dev donations
     */
    function recordDonation(bytes32 personId) public onlyOwner returns(bytes32 donationId) {
        require(personSet.exists(personId), "PersonId not found.");
        PersonStruct storage per = person[personId];
        LongTermInformationStruct storage l = per.longTermInformation;
        ShortTermInformationStruct storage s = per.shortTermInformation;
        require(!l.drugs, "Cannot accept donation tainted with possible drug use.");
        require(!s.tickBite, "Cannot accept donation with possible tick bite-related infection.");
        // Check long and short-term conditions for rejecting donations. This depends on the actual medical rules.
        donationId = genId();
        DonationStruct storage d = donation[donationId];
        d.signer = msg.sender; // record the transaction signer just seems useful, anticipating multiple clinics. Not sure about meta data for the DonationStruct.i
        d.date = now;
        d.expiryDate = now + SHELF_LIFE;
        d.doner = personId;
        per.donationSet.insert(donationId);
        emit LogDonation(msg.sender, personId, d.expiryDate);
    }
    function donationRecord(bytes32 donationId) public view returns(address signer, uint date, uint expiryDate, bytes32 doner, bool destroyed) {
        require(donationSet.exists(donationId), "Donation not found.");
        DonationStruct storage d = donation[donationId];
        return (d.signer, d.date, d.expiryDate, d.doner, d.destroyed);
    }
    function personDonationCount(bytes32 personId) public view returns(uint count) {
        require(personSet.exists(personId), "PersonId not found.");
        return person[personId].donationSet.count();
    }
    function personDonationAtIndex(bytes32 personId, uint index) public view returns(bytes32 donationId) {
        require(personSet.exists(personId), "PersonId not found.");
        return person[personId].donationSet.keyAtIndex(index);
    }
    function donationCount() public view returns(uint count) {
        return donationSet.count();
    }
    function donatationAtIndex(uint index) public view returns(bytes32 donationId) {
        return donationSet.keyAtIndex(index);
    }
    function destroyDonation(bytes32 donationId) public onlyOwner {
        require(donationSet.exists(donationId), "Donation not found.");
        DonationStruct storage d = donation[donationId];
        d.destroyed = true;
        emit LogDonationDestroyed(msg.sender, donationId);
    }
}